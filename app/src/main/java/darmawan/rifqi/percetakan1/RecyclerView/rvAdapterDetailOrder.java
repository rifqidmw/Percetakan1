package darmawan.rifqi.percetakan1.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import darmawan.rifqi.percetakan1.R;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.DataDetailList;

/**
 * Created by yolo on 30/05/18.
 */

public class rvAdapterDetailOrder extends RecyclerView.Adapter<rvAdapterDetailOrder.ViewHolder> {

    private List<DataDetailList> detail;

    public rvAdapterDetailOrder(List<DataDetailList> detail) {
        this.detail = detail;
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_detail, parent, false);
        rvAdapterDetailOrder.ViewHolder itemViewHolder = new rvAdapterDetailOrder.ViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_produk.setText("Produk : " + detail.get(position).getProduk());
        holder.tv_oplah.setText("Oplah : " + detail.get(position).getOplah());
        holder.tv_satuan.setText("Satuan : " + detail.get(position).getSatuan());
        holder.tv_jenis.setText("Jenis Order : " + detail.get(position).getJenis_order());
        holder.tv_panjang.setText("Panjang : " + detail.get(position).getPanjang());
        holder.tv_lebar.setText("Lebar : " + detail.get(position).getLebar());
        holder.tv_hal.setText("Jumlah Halaman Isi : " + detail.get(position).getJml_hlm_isi());
        holder.tv_status.setText("Status : " + detail.get(position).getStatus());

    }

    @Override
    public int getItemCount() {
        return (detail == null) ? 0 : detail.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_produk, tv_oplah, tv_satuan, tv_jenis, tv_panjang, tv_lebar, tv_hal, tv_status;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_produk = itemView.findViewById(R.id.tv_produk);
            tv_oplah = itemView.findViewById(R.id.tv_oplah);
            tv_satuan = itemView.findViewById(R.id.tv_satuan);
            tv_jenis = itemView.findViewById(R.id.tv_jenis);
            tv_panjang = itemView.findViewById(R.id.tv_panjang);
            tv_lebar = itemView.findViewById(R.id.tv_lebar);
            tv_hal = itemView.findViewById(R.id.tv_hal);
            tv_status = itemView.findViewById(R.id.tv_status);
        }
    }
}
