package darmawan.rifqi.percetakan1.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import darmawan.rifqi.percetakan1.MainActivity;
import darmawan.rifqi.percetakan1.R;
import darmawan.rifqi.percetakan1.api.BaseApiService;
import darmawan.rifqi.percetakan1.api.UtilsApi;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Value.ACreatePemesanan;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Value.ADetailPemesanan;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Value.CodeCreatePemesanan;
import darmawan.rifqi.percetakan1.model.CreatePemesanan.Result.ResponseCreatePemesanan;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTambahOrder.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTambahOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTambahOrder extends Fragment {
    public static FragmentTambahOrder newInstance() {
        return new FragmentTambahOrder();
    }
    private DatePickerDialog datePicker;
    private SimpleDateFormat formatDate;
    private static EditText tx_date, et_nama_order, et_pemesan, et_alamat, et_telp, et_ket;
    private static TextInputLayout lay_date, lay_nama_order, lay_pemesan, lay_alamat, lay_telp, lay_ket;
    private static CardView cardview1, cardview2;
    private static ImageButton bt_date;
    private static Button btn_order, add_produk;
    private final CodeCreatePemesanan codeCreatePemesanan = new CodeCreatePemesanan();
    private final ACreatePemesanan aCreatePemesanan = new ACreatePemesanan();
    private final ArrayList<ADetailPemesanan> detail = new ArrayList<ADetailPemesanan>();

    ProgressDialog progressDialog;
    BaseApiService mApiService;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentTambahOrder() {
        // Required empty public constructor
    }

    public static FragmentTambahOrder newInstance(String param1, String param2) {
        FragmentTambahOrder fragment = new FragmentTambahOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_tambah_order, container, false);

        mApiService = UtilsApi.getAPIService();

        tx_date = (EditText)fragmentView.findViewById(R.id.et_date);
        bt_date = (ImageButton)fragmentView.findViewById(R.id.bt_date);
        et_nama_order = (EditText)fragmentView.findViewById(R.id.et_nama_order);
        et_pemesan = (EditText)fragmentView.findViewById(R.id.et_pemesan);
        et_alamat = (EditText) fragmentView.findViewById(R.id.et_alamat);
        et_telp = (EditText) fragmentView.findViewById(R.id.et_telp);
        et_ket = (EditText) fragmentView.findViewById(R.id.et_ket);
        lay_date = (TextInputLayout) fragmentView.findViewById(R.id.lay_date);
        lay_nama_order = (TextInputLayout) fragmentView.findViewById(R.id.lay_nama_order);
        lay_pemesan = (TextInputLayout) fragmentView.findViewById(R.id.lay_pemesan);
        lay_alamat = (TextInputLayout) fragmentView.findViewById(R.id.lay_alamat);
        lay_telp = (TextInputLayout) fragmentView.findViewById(R.id.lay_telp);
        lay_ket = (TextInputLayout) fragmentView.findViewById(R.id.lay_ket);
        cardview1 = (CardView) fragmentView.findViewById(R.id.cv_button1);
        cardview2 = (CardView) fragmentView.findViewById(R.id.cv_button2);

        bt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        cardview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProduk();
            }
        });

        cardview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tx_date.getText().toString().equals("") || et_nama_order.getText().toString().equals("") ||
                        et_pemesan.getText().toString().equals("") || et_alamat.getText().toString().equals("") ||
                        et_telp.getText().toString().equals("") || et_ket.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "Data harus diisi dengan lengkap !!!", Toast.LENGTH_SHORT).show();
                } else {
                    tmbOrder();
                }
                String pemesan, telp, date, nama_order, alamat, ket;
            }
        });

        return fragmentView;
    }

    private void addProduk(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Tambah Produk");

        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.fragment_fragment_tambah_detail, (ViewGroup) getView(), false);

        final EditText et_produk = (EditText) viewInflated.findViewById(R.id.et_produk);
        final EditText et_oplah = (EditText) viewInflated.findViewById(R.id.et_oplah);
        final EditText et_satuan = (EditText) viewInflated.findViewById(R.id.et_satuan);
        final EditText et_jenis_order = (EditText) viewInflated.findViewById(R.id.et_jenis);
        final EditText et_panjang = (EditText) viewInflated.findViewById(R.id.et_panjang);
        final EditText et_lebar = (EditText) viewInflated.findViewById(R.id.et_lebar);
        final EditText et_jml_hlm_isi = (EditText) viewInflated.findViewById(R.id.et_hal);

        builder.setView(viewInflated);


        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String produk, oplah, satuan, jenis_order, panjang, lebar, jml_hal;

                if (et_produk.getText().toString().equals("") || et_oplah.getText().toString().equals("") ||
                        et_satuan.getText().toString().equals("") || et_jenis_order.getText().toString().equals("") ||
                        et_panjang.getText().toString().equals("") || et_lebar.getText().toString().equals("") ||
                        et_jml_hlm_isi.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "Gagal menambahkan produk, data harap diisi dengan lengkap !!!", Toast.LENGTH_SHORT).show();
                } else {
                    produk = et_produk.getText().toString();
                    oplah = et_oplah.getText().toString();
                    satuan = et_satuan.getText().toString();
                    jenis_order = et_jenis_order.getText().toString();
                    panjang = et_panjang.getText().toString();
                    lebar = et_lebar.getText().toString();
                    jml_hal = et_jml_hlm_isi.getText().toString();

                    ADetailPemesanan aDetailPemesanan = new ADetailPemesanan();

                    aDetailPemesanan.setProduk(produk);
                    aDetailPemesanan.setOplah(oplah);
                    aDetailPemesanan.setSatuan(satuan);
                    aDetailPemesanan.setJenis_order(jenis_order);
                    aDetailPemesanan.setPanjang(panjang);
                    aDetailPemesanan.setLebar(lebar);
                    aDetailPemesanan.setJml_hlm_isi(jml_hal);
                    detail.add(aDetailPemesanan);
                    Log.d("Log tambah", detail.toString());

                    Toast.makeText(getActivity(), "Berhasil menambahkan produk "+produk, Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void tmbOrder(){
        String nama_order, pemesan, alamat, notelp, date, ket;
        nama_order = et_nama_order.getText().toString();
        pemesan = et_pemesan.getText().toString();
        alamat = et_alamat.getText().toString();
        notelp = et_telp.getText().toString();
        date = tx_date.getText().toString();
        ket = et_ket.getText().toString();



        String HeaderCode = "0105";
        String signature = "d97f54ee506ce4504c186d7c7e4de62aa1353dd3";
        String apiKey = MainActivity.apiKey;

        codeCreatePemesanan.setHeaderCode(HeaderCode);
        codeCreatePemesanan.setSignature(signature);
        codeCreatePemesanan.setApiKey(apiKey);

        aCreatePemesanan.setNama_order(nama_order);
        aCreatePemesanan.setNama_pemesan(pemesan);
        aCreatePemesanan.setAlamat_pemesan(alamat);
        aCreatePemesanan.setNo_telp_pemesan(notelp);
        aCreatePemesanan.setTgl_order(date);
        aCreatePemesanan.setKet(ket);
        codeCreatePemesanan.setData(aCreatePemesanan);

        aCreatePemesanan.setDetail(detail);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Tambah Order ....");
        progressDialog.show();
        mApiService.createPemesanan(codeCreatePemesanan)
                .enqueue(new Callback<ResponseCreatePemesanan>(){
                    @Override
                    public void onResponse(Call<ResponseCreatePemesanan> call, Response<ResponseCreatePemesanan> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus().equals("200")) {
                                String messsage = response.body().getMessage();

                                Toast.makeText(getActivity(), messsage, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra("apiKey", MainActivity.apiKey);
                                intent.putExtra("username", MainActivity.username);
                                intent.putExtra("alamat", MainActivity.alamat);
                                intent.putExtra("level", MainActivity.level);
                                intent.putExtra("telp", MainActivity.telp);
                                startActivity(intent);
                            } else {
                                String message = response.body().getErrorMessage();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d("pg", "asdasd");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCreatePemesanan> call, Throwable t){
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Gagal menambah Order", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static boolean isEmpty(EditText editText) {

        String input = editText.getText().toString().trim();
        return input.length() == 0;

    }
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }
        public void populateSetDate(int year, int month, int day) {
            tx_date.setText(year+"-"+month+"-"+day);
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
