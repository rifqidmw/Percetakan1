package darmawan.rifqi.percetakan1.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import darmawan.rifqi.percetakan1.MainActivity;
import darmawan.rifqi.percetakan1.R;
import darmawan.rifqi.percetakan1.RecyclerView.rvAdapterDetailOrder;
import darmawan.rifqi.percetakan1.RecyclerView.rvAdapterListOrder;
import darmawan.rifqi.percetakan1.api.BaseApiService;
import darmawan.rifqi.percetakan1.api.UtilsApi;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Value.ADetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Value.CodeDetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.List.Value.CodeListPemesanan;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.DataDetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.DataListPemesanan;
import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.ResponseDetailList;
import darmawan.rifqi.percetakan1.model.ListPemesanan.List.Result.ResponseListPemesanan;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentListOrder extends Fragment{

    private ItemTouchHelper mItemTouchHelper;

    private RecyclerView rvView, rvView1;
    private RecyclerView.Adapter adapter, adapter1;
    private RecyclerView.LayoutManager layoutManager, layoutManager1;
    private List<DataListPemesanan> data = new ArrayList<>();
    private List<DataDetailList> data1 = new ArrayList<>();
    BaseApiService mApiService;
    private Context context;


    public FragmentListOrder() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_order, container, false);

        rvView = (RecyclerView) view.findViewById(R.id.rv_main);
        data = new ArrayList<>();

        rvView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this.getActivity());
        rvView.setLayoutManager(layoutManager);

        adapter = new rvAdapterListOrder(data);
        rvView.setAdapter(adapter);

        mApiService = UtilsApi.getAPIService();

        listPemesanan();
        return view;
    }

    private void listPemesanan(){
        String HeaderCode = "0103";
        String signature = "d97f54ee506ce4504c186d7c7e4de62aa1353dd3";
        String apiKey = MainActivity.apiKey;
        final CodeListPemesanan codeListPemesanan = new CodeListPemesanan();
        codeListPemesanan.setHeaderCode(HeaderCode);
        codeListPemesanan.setSignature(signature);
        codeListPemesanan.setApiKey(apiKey);
        mApiService.listPemesanan(codeListPemesanan)
                .enqueue(new Callback<ResponseListPemesanan>(){
                    @Override
                    public void onResponse(Call<ResponseListPemesanan> call, final Response<ResponseListPemesanan> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getStatus().equals("200")) {
                                data = response.body().getData();
                                adapter = new rvAdapterListOrder(data);
                                rvView.setAdapter(adapter);
                            } else {
                                String message = response.body().getErrorMessage();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d("pg", "asdasd");
                            String message = response.body().getMessage();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                        rvView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

                                public boolean onSingleTapUp(MotionEvent e){
                                    return true;
                                }
                            });

                            @Override
                            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                                View child = rv.findChildViewUnder(e.getX(), e.getY());
                                if (child != null && gestureDetector.onTouchEvent(e)){
                                    int position = rv.getChildAdapterPosition(child);

                                    final String id_order, nosoki, nosp, nama_order, id_penerima, nama_pemesan, alamat, telp, ket, tgl, status;
                                    id_order = data.get(position).getId_order();
                                    nosoki = data.get(position).getNo_soki();
                                    nosp = data.get(position).getNo_sp();
                                    nama_order = data.get(position).getNama_order();
                                    id_penerima = data.get(position).getId_penerima();
                                    nama_pemesan = data.get(position).getNama_pemesan();
                                    alamat = data.get(position).getAlamat_pemesan();
                                    telp = data.get(position).getNo_telp_pemesan();
                                    ket = data.get(position).getKet();
                                    tgl = data.get(position).getTgl_order();
                                    status = data.get(position).getStatus();

                                    final CodeDetailList codeDetailList = new CodeDetailList();
                                    codeDetailList.setHeaderCode("0106");
                                    codeDetailList.setSignature("d97f54ee506ce4504c186d7c7e4de62aa1353dd3");
                                    codeDetailList.setApiKey(MainActivity.apiKey);
                                    ADetailList aDetailList = new ADetailList();
                                    aDetailList.setId_order(id_order);
                                    codeDetailList.setData(aDetailList);
                                    mApiService.detailPemesanan(codeDetailList)
                                            .enqueue(new Callback<ResponseDetailList>() {
                                                @Override
                                                public void onResponse(Call<ResponseDetailList> call, Response<ResponseDetailList> response) {
                                                    if (response.isSuccessful()) {
                                                        if (response.body().getStatus().equals("200")) {

                                                            data1 = response.body().getData();

                                                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                            builder.setTitle("Detail Order");

                                                            View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.fragment_fragment_list_detail, (ViewGroup) getView(), false);

                                                            TextView tv_id_order, tv_nosoki, tv_nosp, tv_nama_order, tv_id_penerima, tv_nama_pemesan, tv_alamat, tv_telp, tv_ket, tv_tgl, tv_status;
                                                            tv_id_order = (TextView) viewInflated.findViewById(R.id.et_id_order);
                                                            tv_nosoki = (TextView) viewInflated.findViewById(R.id.et_nosoki);
                                                            tv_nosp = (TextView) viewInflated.findViewById(R.id.et_nosp);
                                                            tv_nama_order = (TextView) viewInflated.findViewById(R.id.et_nama_order);
                                                            tv_id_penerima = (TextView) viewInflated.findViewById(R.id.et_id_penerima);
                                                            tv_nama_pemesan = (TextView) viewInflated.findViewById(R.id.et_nama_pemesan);
                                                            tv_alamat = (TextView) viewInflated.findViewById(R.id.et_alamat);
                                                            tv_telp = (TextView) viewInflated.findViewById(R.id.et_telp);
                                                            tv_ket = (TextView) viewInflated.findViewById(R.id.et_ket);
                                                            tv_tgl = (TextView) viewInflated.findViewById(R.id.et_tgl);
                                                            tv_status = (TextView) viewInflated.findViewById(R.id.et_status);

                                                            tv_id_order.setText("ID Order : "+id_order);
                                                            tv_nosoki.setText("No. SOKI : "+nosoki);
                                                            tv_nosp.setText("No. SP : "+nosp);
                                                            tv_nama_order.setText("Nama Order : "+nama_order);
                                                            tv_id_penerima.setText("ID Penerima : "+id_penerima);
                                                            tv_nama_pemesan.setText("Nama Pemesan : "+nama_pemesan);
                                                            tv_alamat.setText("Alamat : "+alamat);
                                                            tv_telp.setText("No. Telepon : "+telp);
                                                            tv_ket.setText("Keterangan : "+ket);
                                                            tv_tgl.setText("Tanggal Order : "+tgl);
                                                            tv_status.setText("Status : "+status);

                                                            rvView1 = (RecyclerView) viewInflated.findViewById(R.id.rv_detail);
                                                            rvView1.setHasFixedSize(true);

                                                            layoutManager1 = new LinearLayoutManager(getActivity());
                                                            rvView1.setLayoutManager(layoutManager1);

                                                            adapter1 = new rvAdapterDetailOrder(data1);
                                                            rvView1.setAdapter(adapter1);

                                                            builder.setView(viewInflated);

                                                            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.cancel();
                                                                }
                                                            });

                                                            builder.show();

                                                        } else {
                                                            String message = response.body().getErrorMessage();
                                                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Log.d("pg", "asdasd");
                                                        String message = response.body().getMessage();
                                                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseDetailList> call, Throwable t) {

                                                }
                                            });
                                    /*
                                    final List<DataDetailList> detail;
                                    detail = data.get(position).getDetail();

                                    Bundle bundle = new Bundle();
                                    bundle.putParcelableArrayList("detail", new ArrayList<DataDetailList>(detail));
                                    */
                                }
                                return false;
                            }

                            @Override
                            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                            }

                            @Override
                            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ResponseListPemesanan> call, Throwable t){
                        Log.d("asd", "dasdasda");
                    }
                });
    }
}