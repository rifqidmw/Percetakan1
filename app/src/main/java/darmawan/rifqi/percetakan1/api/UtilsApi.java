package darmawan.rifqi.percetakan1.api;

/**
 * Created by yolo on 15/05/18.
 */

public class UtilsApi {
    public static final String BASE_URL_API = "http://103.100.27.72:5040/api/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
