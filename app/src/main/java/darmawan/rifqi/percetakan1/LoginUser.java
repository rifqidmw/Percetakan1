package darmawan.rifqi.percetakan1;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import darmawan.rifqi.percetakan1.Session.SharedPrefManager;
import darmawan.rifqi.percetakan1.api.BaseApiService;
import darmawan.rifqi.percetakan1.api.UtilsApi;
import darmawan.rifqi.percetakan1.data.StaticConfig;
import darmawan.rifqi.percetakan1.model.Login.LoginUser.Value.CodeUserModel;
import darmawan.rifqi.percetakan1.model.Login.LoginUser.Value.AUserModel;
import darmawan.rifqi.percetakan1.model.Login.LoginUser.Result.ResponseUserModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginUser extends AppCompatActivity {
    Button btn_login;
    EditText etUsername, etPassword;
    ProgressDialog progressDialog;
    Context mContext;
    BaseApiService mApiService;

    SharedPrefManager sharedPrefManager;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_user);

        sharedPrefManager = new SharedPrefManager(this);
        mContext = this;
        mApiService = UtilsApi.getAPIService();
        initComponents();

        if (sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(LoginUser.this, MainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    private void initComponents(){
        btn_login = (Button) findViewById(R.id.btn_login);
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestLogin();
            }
        });
    }

    private void requestLogin(){
        String HeaderCode = "0101";
        String signature = "d97f54ee506ce4504c186d7c7e4de62aa1353dd3";

        CodeUserModel codeUserModel = new CodeUserModel();
        codeUserModel.setHeaderCode(HeaderCode);
        codeUserModel.setSignature(signature);
        AUserModel dataModel = new AUserModel();
        dataModel.setUsername(etUsername.getText().toString());
        dataModel.setPassword(etPassword.getText().toString());
        codeUserModel.setData(dataModel);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Sedang Login ....");
        progressDialog.show();
        mApiService.loginRequest(codeUserModel)
                .enqueue(new Callback<ResponseUserModel>(){
                    @Override
                    public void onResponse(Call<ResponseUserModel> call, Response<ResponseUserModel> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus().equals("200")) {
                                String apiKey = response.body().getData().getApiKey();
                                String nama = response.body().getData().getNama_lengkap();
                                String messsage = response.body().getMessage();
                                String level = response.body().getData().getLevel_text();
                                String alamat = response.body().getData().getAlamat();
                                String telp = response.body().getData().getNo_telp();

                                Toast.makeText(mContext, messsage, Toast.LENGTH_SHORT).show();
                                sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA, apiKey);
                                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                                Intent intent = new Intent(mContext, MainActivity.class);
                                intent.putExtra("apiKey", apiKey);
                                intent.putExtra("username", nama);
                                intent.putExtra("level", level);
                                intent.putExtra("alamat", alamat);
                                intent.putExtra("telp", telp);
                                startActivity(intent);
                            } else {
                                String message = response.body().getErrorMessage();
                                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d("pg", "asdasd");
                            String message = response.body().getMessage();
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseUserModel> call, Throwable t){
                        progressDialog.dismiss();
                    }
                });
    }
}
