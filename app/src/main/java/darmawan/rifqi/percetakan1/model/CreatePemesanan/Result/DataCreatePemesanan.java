package darmawan.rifqi.percetakan1.model.CreatePemesanan.Result;

import java.util.List;

/**
 * Created by yolo on 21/05/18.
 */

public class DataCreatePemesanan {
    String nama_order, id_penerima, nama_pemesan, alamat_pemesan, no_telp_pemesan, tgl_order, ket, status, id_order, no_soki, no_sp;
   List<DataDetailPemesanan> detail;

    public String getNama_order() {
        return nama_order;
    }

    public void setNama_order(String nama_order) {
        this.nama_order = nama_order;
    }

    public String getId_penerima() {
        return id_penerima;
    }

    public void setId_penerima(String id_penerima) {
        this.id_penerima = id_penerima;
    }

    public String getNama_pemesan() {
        return nama_pemesan;
    }

    public void setNama_pemesan(String nama_pemesan) {
        this.nama_pemesan = nama_pemesan;
    }

    public String getAlamat_pemesan() {
        return alamat_pemesan;
    }

    public void setAlamat_pemesan(String alamat_pemesan) {
        this.alamat_pemesan = alamat_pemesan;
    }

    public String getNo_telp_pemesan() {
        return no_telp_pemesan;
    }

    public void setNo_telp_pemesan(String no_telp_pemesan) {
        this.no_telp_pemesan = no_telp_pemesan;
    }

    public String getTgl_order() {
        return tgl_order;
    }

    public void setTgl_order(String tgl_order) {
        this.tgl_order = tgl_order;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getNo_soki() {
        return no_soki;
    }

    public void setNo_soki(String no_soki) {
        this.no_soki = no_soki;
    }

    public String getNo_sp() {
        return no_sp;
    }

    public void setNo_sp(String no_sp) {
        this.no_sp = no_sp;
    }

    public List<DataDetailPemesanan> getDetail() {
        return detail;
    }

    public void setDetail(List<DataDetailPemesanan> detail) {
        this.detail = detail;
    }
}
