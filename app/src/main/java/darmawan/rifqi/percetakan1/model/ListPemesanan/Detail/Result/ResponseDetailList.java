package darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result;

import java.util.List;

/**
 * Created by yolo on 04/06/18.
 */

public class ResponseDetailList {
    String status, message, errorMessage;
    List<DataDetailList> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<DataDetailList> getData() {
        return data;
    }

    public void setData(List<DataDetailList> data) {
        this.data = data;
    }
}
