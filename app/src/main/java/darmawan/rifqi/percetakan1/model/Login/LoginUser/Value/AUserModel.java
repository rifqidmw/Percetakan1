package darmawan.rifqi.percetakan1.model.Login.LoginUser.Value;

/**
 * Created by yolo on 15/05/18.
 */

public class AUserModel {
    String username, password, nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}