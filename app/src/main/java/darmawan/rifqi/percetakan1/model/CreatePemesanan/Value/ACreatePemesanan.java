package darmawan.rifqi.percetakan1.model.CreatePemesanan.Value;

import java.util.List;

/**
 * Created by yolo on 21/05/18.
 */

public class ACreatePemesanan {
    String nama_order, nama_pemesan, alamat_pemesan, no_telp_pemesan, tgl_order, ket;
    List<ADetailPemesanan> detail;

    public String getNama_order() {
        return nama_order;
    }

    public void setNama_order(String nama_order) {
        this.nama_order = nama_order;
    }

    public String getNama_pemesan() {
        return nama_pemesan;
    }

    public void setNama_pemesan(String nama_pemesan) {
        this.nama_pemesan = nama_pemesan;
    }

    public String getAlamat_pemesan() {
        return alamat_pemesan;
    }

    public void setAlamat_pemesan(String alamat_pemesan) {
        this.alamat_pemesan = alamat_pemesan;
    }

    public String getNo_telp_pemesan() {
        return no_telp_pemesan;
    }

    public void setNo_telp_pemesan(String no_telp_pemesan) {
        this.no_telp_pemesan = no_telp_pemesan;
    }

    public String getTgl_order() {
        return tgl_order;
    }

    public void setTgl_order(String tgl_order) {
        this.tgl_order = tgl_order;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public List<ADetailPemesanan> getDetail() {
        return detail;
    }

    public void setDetail(List<ADetailPemesanan> detail) {
        this.detail = detail;
    }
}
