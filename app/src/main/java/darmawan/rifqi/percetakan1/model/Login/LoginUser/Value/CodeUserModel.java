package darmawan.rifqi.percetakan1.model.Login.LoginUser.Value;

/**
 * Created by yolo on 15/05/18.
 */

public class CodeUserModel {
    String HeaderCode, signature;
    AUserModel data;

    public String getHeaderCode() {
        return HeaderCode;
    }

    public String getSignature() {
        return signature;
    }

    public AUserModel getData() {
        return data;
    }

    public void setHeaderCode(String headerCode) {
        HeaderCode = headerCode;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public void setData(AUserModel data) {
        this.data = data;
    }
}

