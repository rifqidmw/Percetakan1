package darmawan.rifqi.percetakan1.model.CreatePemesanan.Result;

/**
 * Created by yolo on 21/05/18.
 */

public class DataDetailPemesanan {
    String id_detail_order, id_order, produk, oplah, satuan, jenis_order, panjang, lebar, jml_hlm_isi, status, finish_pracetak, finish_bahan, finish_cetak, finish_finishing;

    public String getId_detail_order() {
        return id_detail_order;
    }

    public void setId_detail_order(String id_detail_order) {
        this.id_detail_order = id_detail_order;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getOplah() {
        return oplah;
    }

    public void setOplah(String oplah) {
        this.oplah = oplah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getJenis_order() {
        return jenis_order;
    }

    public void setJenis_order(String jenis_order) {
        this.jenis_order = jenis_order;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getJml_hlm_isi() {
        return jml_hlm_isi;
    }

    public void setJml_hlm_isi(String jml_hlm_isi) {
        this.jml_hlm_isi = jml_hlm_isi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinish_pracetak() {
        return finish_pracetak;
    }

    public void setFinish_pracetak(String finish_pracetak) {
        this.finish_pracetak = finish_pracetak;
    }

    public String getFinish_bahan() {
        return finish_bahan;
    }

    public void setFinish_bahan(String finish_bahan) {
        this.finish_bahan = finish_bahan;
    }

    public String getFinish_cetak() {
        return finish_cetak;
    }

    public void setFinish_cetak(String finish_cetak) {
        this.finish_cetak = finish_cetak;
    }

    public String getFinish_finishing() {
        return finish_finishing;
    }

    public void setFinish_finishing(String finish_finishing) {
        this.finish_finishing = finish_finishing;
    }
}
