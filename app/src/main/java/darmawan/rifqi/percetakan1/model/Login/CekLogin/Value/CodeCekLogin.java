package darmawan.rifqi.percetakan1.model.Login.CekLogin.Value;

/**
 * Created by yolo on 04/06/18.
 */

public class CodeCekLogin {
    String HeaderCode, signature;
    ACekLogin data;

    public String getHeaderCode() {
        return HeaderCode;
    }

    public void setHeaderCode(String headerCode) {
        HeaderCode = headerCode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public ACekLogin getData() {
        return data;
    }

    public void setData(ACekLogin data) {
        this.data = data;
    }
}
