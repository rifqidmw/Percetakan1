package darmawan.rifqi.percetakan1.model.Login.CekLogin.Result;

/**
 * Created by yolo on 04/06/18.
 */

public class ResponseCekLogin {
    String status, message, errorMessage;
    DataCekLogin data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public DataCekLogin getData() {
        return data;
    }

    public void setData(DataCekLogin data) {
        this.data = data;
    }
}
