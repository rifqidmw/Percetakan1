package darmawan.rifqi.percetakan1.model.ListPemesanan.List.Value;

/**
 * Created by yolo on 26/05/18.
 */

public class CodeListPemesanan {
    String HeaderCode, signature, apiKey;

    public String getHeaderCode() {
        return HeaderCode;
    }

    public void setHeaderCode(String headerCode) {
        HeaderCode = headerCode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
