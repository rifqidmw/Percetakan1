package darmawan.rifqi.percetakan1.model.Login.LoginUser.Result;

/**
 * Created by yolo on 15/05/18.
 */

public class ResponseUserModel {
    String status, message, errorMessage;
    DataUserModel data;

    public DataUserModel getData() {
        return data;
    }

    public void setData(DataUserModel data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
