package darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yolo on 26/05/18.
 */

public class DataDetailList implements Parcelable {
    String id_detail_order, id_order, produk, oplah, satuan, jenis_order, panjang, lebar, jml_hlm_isi, status, finish_pracetak, finish_bahan, finish_cetak, finish_finishing;

    public String getId_detail_order() {
        return id_detail_order;
    }

    public void setId_detail_order(String id_detail_order) {
        this.id_detail_order = id_detail_order;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getOplah() {
        return oplah;
    }

    public void setOplah(String oplah) {
        this.oplah = oplah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getJenis_order() {
        return jenis_order;
    }

    public void setJenis_order(String jenis_order) {
        this.jenis_order = jenis_order;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getJml_hlm_isi() {
        return jml_hlm_isi;
    }

    public void setJml_hlm_isi(String jml_hlm_isi) {
        this.jml_hlm_isi = jml_hlm_isi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinish_pracetak() {
        return finish_pracetak;
    }

    public void setFinish_pracetak(String finish_pracetak) {
        this.finish_pracetak = finish_pracetak;
    }

    public String getFinish_bahan() {
        return finish_bahan;
    }

    public void setFinish_bahan(String finish_bahan) {
        this.finish_bahan = finish_bahan;
    }

    public String getFinish_cetak() {
        return finish_cetak;
    }

    public void setFinish_cetak(String finish_cetak) {
        this.finish_cetak = finish_cetak;
    }

    public String getFinish_finishing() {
        return finish_finishing;
    }

    public void setFinish_finishing(String finish_finishing) {
        this.finish_finishing = finish_finishing;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id_detail_order);
        dest.writeString(this.id_order);
        dest.writeString(this.produk);
        dest.writeString(this.oplah);
        dest.writeString(this.satuan);
        dest.writeString(this.jenis_order);
        dest.writeString(this.panjang);
        dest.writeString(this.lebar);
        dest.writeString(this.jml_hlm_isi);
        dest.writeString(this.status);
        dest.writeString(this.finish_pracetak);
        dest.writeString(this.finish_bahan);
        dest.writeString(this.finish_cetak);
        dest.writeString(this.finish_finishing);
    }

    public DataDetailList() {
    }

    protected DataDetailList(Parcel in) {
        this.id_detail_order = in.readString();
        this.id_order = in.readString();
        this.produk = in.readString();
        this.oplah = in.readString();
        this.satuan = in.readString();
        this.jenis_order = in.readString();
        this.panjang = in.readString();
        this.lebar = in.readString();
        this.jml_hlm_isi = in.readString();
        this.status = in.readString();
        this.finish_pracetak = in.readString();
        this.finish_bahan = in.readString();
        this.finish_cetak = in.readString();
        this.finish_finishing = in.readString();
    }

    public static final Parcelable.Creator<DataDetailList> CREATOR = new Parcelable.Creator<DataDetailList>() {
        @Override
        public DataDetailList createFromParcel(Parcel source) {
            return new DataDetailList(source);
        }

        @Override
        public DataDetailList[] newArray(int size) {
            return new DataDetailList[size];
        }
    };

}
