package darmawan.rifqi.percetakan1.model.CreatePemesanan.Value;

/**
 * Created by yolo on 21/05/18.
 */

public class CodeCreatePemesanan {
    String HeaderCode, signature, apiKey;
    ACreatePemesanan data;

    public String getHeaderCode() {
        return HeaderCode;
    }

    public void setHeaderCode(String headerCode) {
        HeaderCode = headerCode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public ACreatePemesanan getData() {
        return data;
    }

    public void setData(ACreatePemesanan data) {
        this.data = data;
    }
}
