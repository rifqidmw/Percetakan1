package darmawan.rifqi.percetakan1.model.ListPemesanan.List.Result;

import java.util.List;

import darmawan.rifqi.percetakan1.model.ListPemesanan.Detail.Result.DataListPemesanan;

/**
 * Created by yolo on 26/05/18.
 */

public class ResponseListPemesanan {
    String status, message, errorMessage;
    List<DataListPemesanan> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<DataListPemesanan> getData() {
        return data;
    }

    public void setData(List<DataListPemesanan> data) {
        this.data = data;
    }

}
