package darmawan.rifqi.percetakan1;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import darmawan.rifqi.percetakan1.Fragment.FragmentListOrder;
import darmawan.rifqi.percetakan1.Fragment.FragmentProfile;
import darmawan.rifqi.percetakan1.Fragment.FragmentTambahOrder;
import darmawan.rifqi.percetakan1.Session.SharedPrefManager;
import darmawan.rifqi.percetakan1.api.BaseApiService;
import darmawan.rifqi.percetakan1.api.UtilsApi;
import darmawan.rifqi.percetakan1.model.Login.CekLogin.Value.ACekLogin;
import darmawan.rifqi.percetakan1.model.Login.CekLogin.Value.CodeCekLogin;
import darmawan.rifqi.percetakan1.model.Login.CekLogin.Result.ResponseCekLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements
        FragmentTambahOrder.OnFragmentInteractionListener,
        FragmentProfile.OnFragmentInteractionListener{
    BottomNavigationView bottomNavigationView;
    public static String apiKey, username, level, alamat, telp;
    BaseApiService mApiService;
    SharedPrefManager sharedPrefManager;
    @Override
    public void onFragmentInteraction(Uri uri){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPrefManager = new SharedPrefManager(this);

        Intent intent = getIntent();
        String apiKey1 = intent.getStringExtra("apiKey");
        String username1 = intent.getStringExtra("username");
        String level1 = intent.getStringExtra("level");
        String alamat1 = intent.getStringExtra("alamat");
        String telp1 = intent.getStringExtra("telp");
        apiKey = apiKey1;
        username = username1;
        level = level1;
        alamat = alamat1;
        telp = telp1;

        Intent intent1 = getIntent();
        String apiKey2 = intent1.getStringExtra("apiKey");
        String username2 = intent1.getStringExtra("username");
        String level2 = intent1.getStringExtra("level");
        String alamat2 = intent1.getStringExtra("alamat");
        String telp2 = intent1.getStringExtra("telp");
        apiKey = apiKey2;
        username = username2;
        level = level2;
        alamat = alamat2;
        telp = telp2;

        mApiService = UtilsApi.getAPIService();
        CodeCekLogin codeCekLogin = new CodeCekLogin();
        codeCekLogin.setHeaderCode("0102");
        codeCekLogin.setSignature("d97f54ee506ce4504c186d7c7e4de62aa1353dd3");
        ACekLogin aCekLogin = new ACekLogin();
        aCekLogin.setToken(apiKey1);
        codeCekLogin.setData(aCekLogin);
        mApiService.cekLogin(codeCekLogin)
                .enqueue(new Callback<ResponseCekLogin>() {
                    @Override
                    public void onResponse(Call<ResponseCekLogin> call, Response<ResponseCekLogin> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseCekLogin> call, Throwable t) {
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        startActivity(new Intent(MainActivity.this, LoginUser.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.bn_order:
                                selectedFragment = FragmentTambahOrder.newInstance();
                                break;
                            case R.id.bn_list:
                                selectedFragment = new FragmentListOrder();
                                break;
                            case R.id.bn_profile:
                                selectedFragment = FragmentProfile.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.flContent, selectedFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        return true;
                    }
                });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, new FragmentListOrder());
        transaction.commit();
    }
}
